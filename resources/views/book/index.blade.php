
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<div class='container'>
    <br/><br/>
      <table class="table table-bordered">
          <thead class="thead-dark">
            <tr>
              <th scope="col">Id</th>
              <th scope="col"> Book title</th>
              <th scope="col">Author name</th>
              <th scope="col">Created at</th>
              <th scope="col">Updated at</th>
              
            </tr>
          </thead>
      
          <tbody>
          @foreach($books as $book)
            <tr>
                <td>{{$book->id}}</td>
                <td>{{$book->title}}</td>
                <td>{{$book->author}}</td>
                <td>{{$book->created_at}}</td> 
                <td>{{$book->updated_at}}</td>
                
            </tr>
            @endforeach

          </tbody>
    