<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert(
        [
            [
                'title' => 'Toy story',
                'author' => 'Matan Chover',
                'user_id'=> 1 , 
                'created_at' =>date('Y-m-d G:i:s'),
                'updated_at'=>date('Y-m-d G:i:s'),
            ],
            [
                'title' => 'Tot story 2',
                'author' => 'Matan Chover',
                'user_id'=> 1 , 
                'created_at' =>date('Y-m-d G:i:s'),
                'updated_at'=>date('Y-m-d G:i:s'),
            ],
            [
                'title' => 'Boy',
                'author' => 'Eliran Srur',
                'user_id'=> 2 , 
                'created_at' =>date('Y-m-d G:i:s'),
                'updated_at'=>date('Y-m-d G:i:s'),
            ],
            [
                'title' => 'Boy 2',
                'author' => 'Eliran Srur',
                'user_id'=> 2 , 
                'created_at' =>date('Y-m-d G:i:s'),
                'updated_at'=>date('Y-m-d G:i:s'),
            ]
        ]

    );
    }
}
